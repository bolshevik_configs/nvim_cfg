return {
    'tiagovla/tokyodark.nvim',
    config = function()
        local theme = require("tokyodark")
        theme.setup({
            transparent_background = false, -- set background to transparent
            gamma = 0.9, -- adjust the brightness of the theme
            custom_palette = {grey = "#0d3912"} or function(palette) return {} end, -- extend palette
        })
        vim.cmd [[colorscheme tokyodark]]
    end,
}
