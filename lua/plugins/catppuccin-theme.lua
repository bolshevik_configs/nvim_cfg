-- Readme:
--      https://github.com/catppuccin/nvim?tab=readme-ov-file
return {
    "catppuccin/nvim",
    name = "catppuccin",
    priority = 1000,
    config = function()
        local theme = require("catppuccin")
        theme.setup({
            background = { -- :h background
                light = "latte",
                dark = "mocha",
            },
            dim_inactive = {
                enabled = false, -- dims the background color of inactive window
                shade = "dark",
                percentage = 0.15, -- percentage of the shade to apply to the inactive window
            },
            integrations = {
                cmp = true,
                gitsigns = true,
                telescope = true,
                nvimtree = true,
                treesitter = true,
                notify = true,
                mini = {
                    enabled = true,
                    indentscope_color = "",
                },
                -- For more plugins integrations please scroll down (https://github.com/catppuccin/nvim#integrations)
            },
        })
    end,
}
