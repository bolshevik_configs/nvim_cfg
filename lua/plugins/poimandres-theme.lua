return {
    'olivercederborg/poimandres.nvim',
    lazy = false,
    priority = 1000,
    config = function()
        local theme = require("poimandres")
        theme.setup({

        })
    end,
}
