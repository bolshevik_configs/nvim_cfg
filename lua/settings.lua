require "helpers/globals"
require "base/search"
require "base/tabs"
require "autocmds"

require('keys/main')
require('keys/plugins/tagbar')
require "nolazy_plugins_cfg/main_cfg"

---------- Настройка панелей 
-- Вертикальные сплиты становятся справа
-- По умолчанию панели в Neovim ставятся в зависимости от расположения текущей панели. Данная настройка поможет нам держать панели в порядке
opt.splitright = false
-- Горизонтальные сплиты становятся снизу
opt.splitbelow = true


---------- Дополнительные настройки 
-- Используем системный буфер обмена
-- opt.clipboard = 'unnamedplus'
-- Отключаем дополнение файлов в конце
opt.fixeol = false
-- Автодополнение (встроенное в Neovim)
opt.completeopt = 'menuone,noselect'
-- Не автокомментировать новые линии при переходе на новую строку
-- vim.cmd("autocmd BufEnter * set fo-=c fo-=r fo-=o")


opt.colorcolumn="80"                -- подсвечивать вертикальную линию
opt.textwidth=80                    -- автоперенос строк при превышении лимита

opt.number=true                     -- показывать номера строк
opt.syntax=on                       -- включить подсветку синтаксиса
opt.cursorline=false                -- подсветить текущую строку
opt.list=true                       -- показывать скрытые символы
opt.wrap=false                      -- не заворачивать строки не влезающие в экран
opt.listchars:append "space:⋅"      -- установить сивол отображения пробела
opt.listchars:append "eol:↴"        -- установить сивол отображения конца строки
opt.listchars:append "tab:→-"       -- установить сивол отображения таба

vim.cmd("colorscheme catppuccin")

---------- Сохранение истории undo между сессиями
undodir="~/.config/nvim/.undo"
os.execute("mkdir -p " .. undodir)
vim.cmd("set undofile")
vim.cmd("set undodir=".. undodir)
