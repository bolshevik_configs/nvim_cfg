return {
    'tiagovla/tokyodark.nvim',
    lazy = false,
    priority = 1000,
    config = function()
        local theme = require("tokyodark")
        theme.setup({

        })
        vim.cmd [[colorscheme tokyodark]]
    end,
}
