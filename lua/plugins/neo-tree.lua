return {
    "nvim-neo-tree/neo-tree.nvim",
    dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
        "MunifTanjim/nui.nvim",
        -- "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
    },
    config = function()
        local ntree = require("neo-tree")
        ntree.setup({
            filesystem = {
                filtered_items = {
                    visible = true,
                    show_hidden_count = true,
                    hide_dotfiles = false,
                    hide_gitignored = false,
                    hide_by_name = {
                      -- '.git',
                      -- '.DS_Store',
                      -- 'thumbs.db',
                    },
                    never_show = {},
                },
                follow_current_file = {
                    enabled = true, -- показывать кусок дерева соответств. открытому файлу 
                },
                use_libuv_file_watcher = true, -- следить за файловой системой
            },
            window = {
                mappings = {
                    -- отключить открытие/закрытие субдиректории при нажатии на
                    -- клавиши перемещения курсора
                    ["l"] = "",
                    -- отключить нечеткий поиск в дереве 
                    ["/"] = "",
                    ["h"] = "",
                    -- отключение встроенной функции копирования, дабы можно
                    -- было скопировать имя файла
                    ["y"] = "",
                },
            },
        })
    end,
}

