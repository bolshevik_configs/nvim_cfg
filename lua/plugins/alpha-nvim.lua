return {
  "goolord/alpha-nvim",
  event = "VimEnter",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  config = function()
    local alpha = require("alpha")
    local dashboard = require("alpha.themes.dashboard")

    dashboard.section.header.val = {
    "                     ██    ██    ██                ",
    "                     ██    ██    ██                ",
    "                     ██    ██    ██                ",
    "              ████████████████████████████         ",
    "              ██                        ██         ",
    "              ██                        ██         ",
    "       █████████          █             █████████  ",
    "              ██   █   █     ██   ██    ██         ",
    "              ██   █   █  █  █ █ █ █    ██         ",
    "       █████████    █ █   █  █  █  █    █████████  ",
    "              ██     █    █  █     █    ██         ",
    "              ██                        ██         ",
    "       █████████                        █████████  ",
    "              ██                        ██         ",
    "              ██                        ██         ",
    "              ████████████████████████████         ",
    "                     ██    ██    ██                ",
    "                     ██    ██    ██                ",
    "                     ██    ██    ██                ",
    }

    dashboard.section.buttons.val = {
      dashboard.button("e", "  > New File", "<cmd>ene<CR>"),
      dashboard.button("\\ ff", "󰱼  > Find File", "<cmd>Telescope find_files<CR>"),
      dashboard.button("\\ fg", "󰱼  > Find String", "<cmd>Telescope live_grep<CR>"),
      dashboard.button("\\ fs", "󰱼  > Find Symbol", "<cmd>Telescope lsp_dynamic_workspace_symbols<CR>"),
      dashboard.button("\\ fr", "  > Find Recent", "<cmd>Telescope oldfiles<CR>"),
      dashboard.button("\\ fh", "  > Find plugin docs", "<cmd>Telescope help_tags<CR>"),
      dashboard.button("q", "  > Quit", "<cmd>qa<CR>"),
    }

    alpha.setup(dashboard.opts)

    vim.cmd([[autocmd FileType alpha setlocal nofoldenable]])
  end,
}


