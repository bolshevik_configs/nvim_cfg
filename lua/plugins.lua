return {
    "nvim-lualine/lualine.nvim",
    { 
      "sudormrfbin/cheatsheet.nvim", 
      dependencies = {
        "nvim-telescope/telescope.nvim"
      }
    },
    'mrjones2014/smart-splits.nvim',
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    "neovim/nvim-lspconfig",
    "majutsushi/tagbar",
    "sakhnik/nvim-gdb",
    "rcarriga/nvim-notify",
}
