local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  --vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>a', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', function()
    vim.lsp.buf.format { async = true }
  end, bufopts)
end

local lsp_flags = {
  -- This is the default in Nvim 0.7+
  debounce_text_changes = 150,
}


return {
    "neovim/nvim-lspconfig",
    config = function()
        local lspcfg = require("lspconfig")
        local cmp_cap = require('cmp_nvim_lsp').default_capabilities()

        -- Работает, есть навигация, но при ifdef'ах может начать тупить.
        -- Не работает переход по пакетам, не работают интерфейсы.
        -- Для работы требуется создание файла verible.filelist (см. доку)
        -- find . -name "*.sv" -o -name "*.svh" -o -name "*.v" | sort > verible.filelist
        --
        lspcfg.verible.setup({
            on_attach = on_attach,
            flags = lsp_flags,
            capabilities = cmp_cap,
            filetypes = { "verilog", "systemverilog" },
            cmd = {'verible-verilog-ls', '--rules_config_search'},
            root_dir = function() return vim.loop.cwd() end
        })

        -- Работает, но галюцинирует: путет строки куда нужно перепрыгнуть при
        -- переходе goto, и при возврате. Иногда может попытаться перепрыгнуть
        -- за конец файла, вызывая ошибки. Работает с интерфейсами, не работает
        -- переход по пакетам. Жрет больше CPU, чем verible
        --
        -- lspcfg.veridian.setup({
        --       on_attach = on_attach,
        --       flags = flags,
        --       capabilities = lsp_capabilities,
        --       root_dir = function() return vim.loop.cwd() end
        -- })

        -- Не может ходить по определениям, выкидывает ошибку что функция не
        -- поддерживается
        --
        -- lspcfg.svls.setup({
        --     on_attach = on_attach,
        --     cmd = {"svls", "-d"},
        --     filetypes = { "verilog", "systemverilog" },
        --     -- root_dir = function() return vim.loop.cwd() end
        --     -- root_dir = function(fname)
        --     --   return require("lspconfig.util").find_git_ancestor(fname)
        --     -- end,
        -- })

        lspcfg.bashls.setup({
            on_attach = on_attach,
            flags = lsp_flags,
            capabilities = cmp_cap,
            filetypes = { "sh", "bash" },
            cmd = {'bash-language-server'},
            root_dir = function() return vim.loop.cwd() end
        })

        lspcfg.clangd.setup({
            on_attach = on_attach,
            capabilities = cmp_cap,
        })

        lspcfg.pyright.setup({
            on_attach = on_attach,
            capabilities = cmp_cap,
        })
        -- lspcfg.pylsp.setup({
        --     on_attach = on_attach,
        --     capabilities = cmp_cap,
        --     init_options = {
        --         codeAction = {
        --             nameExtractVariable = "jls_extract_var",
        --             nameExtractFunction = "jls_extract_def",
        --         },
        --         completion = {
        --             disableSnippets = false,
        --             resolveEagerly = false,
        --             ignorePatterns = {},
        --         },
        --         diagnostics = {
        --             enable = true,
        --             didOpen = true,
        --             didChange = true,
        --             didSave = true,
        --         },
        --         hover = {
        --             enable = true,
        --             disable = {
        --                 class = { all = false, names = {}, fullNames = {} },
        --                 ["function"] = { all = false, names = {}, fullNames = {} },
        --                 instance = { all = false, names = {}, fullNames = {} },
        --                 keyword = { all = false, names = {}, fullNames = {} },
        --                 module = { all = false, names = {}, fullNames = {} },
        --                 param = { all = false, names = {}, fullNames = {} },
        --                 path = { all = false, names = {}, fullNames = {} },
        --                 property = { all = false, names = {}, fullNames = {} },
        --                 statement = { all = false, names = {}, fullNames = {} },
        --             },
        --         },
        --         jediSettings = {
        --             autoImportModules = {},
        --             caseInsensitiveCompletion = true,
        --             debug = false,
        --         },
        --         markupKindPreferred = "markdown",
        --         workspace = {
        --             extraPaths = {},
        --             symbols = {
        --                 ignoreFolders = { ".nox", ".tox", ".venv", "__pycache__", "venv" },
        --                 maxSymbols = 20,
        --             },
        --         },
        --     },
        -- })

    end,
}
