require "helpers/globals"

-- если ти файла Makefile, то табы не должны менятся на проблеы
vim.api.nvim_create_autocmd(
    {
        "filetype"
    },
    {
        pattern = "Makefile",
        callback = function()
            opt.expandtab=false
            opt.cin=false
        end
    }
)

vim.api.nvim_create_autocmd(
    {
        "filetype"
    },
    {
        pattern = "*.c, *.cpp",
        callback = function()
            opt.cin=true
        end
    }
)

-- Запуск NvimTree при старте
vim.api.nvim_create_autocmd(
    {
        "VimEnter"
    },
    {
        -- открыть, закрыть, открыть - иначе при запуске vim к окну с neotree не
        -- применится специальная цветовая схема. Почему-то она активируется
        -- тлько если уйти из окна, а потом вернуться
        callback = function()
            vim.cmd("Neotree left")
            vim.cmd("Neotree close")
            vim.cmd("Neotree left")
        end
    }
)

