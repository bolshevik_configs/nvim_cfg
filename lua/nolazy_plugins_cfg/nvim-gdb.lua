--
-- Открывать таб с терминалом gdb справа, вместо того чтоб открыть снизу
--
vim.cmd('let w:nvimgdb_termwin_command = "belowright vnew"')
vim.cmd('let w:nvimgdb_codewin_command = "vnew"')

vim.api.nvim_create_autocmd(
    {
        "VimEnter"
    },
    {

        callback = function()
            if vim.g.nvmgdb_gdb_target_file == nil then
                vim.api.nvim_set_keymap('n', '<leader>dd', 
                    ':GdbStart gdb-multiarch -q ', { noremap = true })
            else
                vim.api.nvim_set_keymap('n', '<leader>dd', 
                    ':GdbStart gdb-multiarch -q '.. 
                    vim.g.nvmgdb_gdb_target_file .. '<CR>', { noremap = true }
                )
            end

            if vim.g.nvmgdb_pbd_target_file ~= nil then
                vim.api.nvim_set_keymap('n', '<leader>dp',
                    ':GdbStartPDB python -m pdb ' ..
                    vim.g.nvmgdb_pbd_target_file .. '<CR>' , { noremap = true }
                )
            end

            if vim.g.nvmgdb_bashdb_target_file ~= nil then
                vim.api.nvim_set_keymap('n', '<leader>db',
                    ':GdbStartBashDB bashdb  ' ..
                    vim.g.nvmgdb_bashdb_target_file ..
                    '<CR>' , { noremap = true }
                )
            end
        end
    }
)

