return {
    "mfussenegger/nvim-dap",
    enabled = vim.fn.has "win32" == 0,
    dependencies = {
        {
            "jay-babu/mason-nvim-dap.nvim",
            dependencies = { "nvim-dap" },
            cmd = { "DapInstall", "DapUninstall" },
            opts = { handlers = {} },
        },
        {
            "nvim-neotest/nvim-nio",
        },
        {
            "rcarriga/nvim-dap-ui",
            opts = { floating = { border = "rounded" } },
            -- config = require "plugins.configs.nvim-dap-ui",
        },
        {
            "rcarriga/cmp-dap",
            dependencies = { "nvim-cmp" },
            -- config = require "plugins.configs.cmp-dap",
        },
    },
    config = function()
        local dap, dapui = require("dap"), require("dapui")
        dapui.setup({

        })
        dap.listeners.after.event_initialized["dapui_config"] = function()
            dapui.open()
        end
        dap.listeners.before.event_terminated["dapui_config"] = function()
            dapui.close()
        end
        dap.listeners.before.event_exited["dapui_config"] = function()
            dapui.close()
        end
    end,
}
